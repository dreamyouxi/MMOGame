#include "AppDelegate.h"
#include "CCLuaEngine.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
#include "lua_module_register.h"
#include "Socket.h"
#if (CC_TARGET_PLATFORM != CC_PLATFORM_LINUX)
#include "ide-support/CodeIDESupport.h"
#endif

#if (COCOS2D_DEBUG > 0) && (CC_CODE_IDE_DEBUG_SUPPORT > 0)
#include "runtime/Runtime.h"
#include "ide-support/RuntimeLuaImpl.h"
#endif

using namespace CocosDenshion;

USING_NS_CC;
using namespace std;

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate()
{
	SimpleAudioEngine::end();
#if (COCOS2D_DEBUG > 0) && (CC_CODE_IDE_DEBUG_SUPPORT > 0)
	// NOTE:Please don't remove this call if you want to debug with Cocos Code IDE
	RuntimeEngine::getInstance()->end();
#endif

}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
	//set OpenGL context attributions,now can only set six attributions:
	//red,green,blue,alpha,depth,stencil
	GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };

	GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages,
// don't modify or remove this function
static int register_all_packages()
{
	return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching()
{
	// set default FPS
	Director::getInstance()->setAnimationInterval(1.0 / 60.0f);

	// register lua module
	auto engine = LuaEngine::getInstance();
	ScriptEngineManager::getInstance()->setScriptEngine(engine);
	lua_State* L = engine->getLuaStack()->getLuaState();
	lua_module_register(L);
	lua_module_register_customs(L);
	register_all_packages();

	LuaStack* stack = engine->getLuaStack();
	stack->setXXTEAKeyAndSign("2dxLua", strlen("2dxLua"), "XXTEA", strlen("XXTEA"));
	// for exe run
	FileUtils::getInstance()->addSearchPath("../../src");
	FileUtils::getInstance()->addSearchPath("../../res");
	FileUtils::getInstance()->addSearchPath("../../");

	//register custom function
	//LuaStack* stack = engine->getLuaStack();
	//register_custom_function(stack->getLuaState());

#if (COCOS2D_DEBUG > 0) && (CC_CODE_IDE_DEBUG_SUPPORT > 0)
	// NOTE:Please don't remove this call if you want to debug with Cocos Code IDE
	auto runtimeEngine = RuntimeEngine::getInstance();
	runtimeEngine->addRuntime(RuntimeLuaImpl::create(), kRuntimeEngineLua);
	runtimeEngine->start();
#else
	if (engine->executeScriptFile("src/main.lua"))
	{
		return false;
	}
#endif
	Socket::GetInstance();


	Director::getInstance()->getScheduler()->scheduleUpdate<AppDelegate>(this, 0, false);
	return true;
}

void AppDelegate::update(float dt)
{
	auto engine = LuaEngine::getInstance();
	lua_State* l = engine->getLuaStack()->getLuaState();

	if (l)
	{
		lua_pushnumber(l, (lua_Number)dt*1000000.0f);
		lua_setglobal(l, "g_UpdateLuaDt");
	}
	engine->executeGlobalFunction("Update_Lua");
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
	Director::getInstance()->stopAnimation();

	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();

	auto engine = LuaEngine::getInstance();
	if (engine)
		engine->executeGlobalFunction("applicationDidEnterBackground");
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
	Director::getInstance()->startAnimation();
	SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	auto engine = LuaEngine::getInstance();
	if (engine)
		engine->executeGlobalFunction("applicationWillEnterForeground");
}
