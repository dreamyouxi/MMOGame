﻿#include "LuaMgr.h"
#include "lua.h"
#include <iostream>
#include "cocos2d.h"
#include "CCLuaEngine.h"
using namespace  cocos2d;
using namespace std;
SingletonCPP(LuaMgr);

void LuaMgr::CallGlobalFunction(const std::string &name)
{
	LuaEngine::getInstance()->getLuaStack()->executeGlobalFunction(name.c_str());
	/*
	LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
	if (!stack)return;
	lua_State* ss = stack->getLuaState();
	if (!ss)return;
	lua_getglobal(ss, "CALL_GLOBAL_FUNCTION");
	lua_pushstring(ss,name.c_str());
	lua_call(ss, 1, 0);
	lua_pop(ss, 1);*/
}
/*
void  LuaMgr::beginCallGlobal(const char*name)
{
	lua_State* ss = LuaEngine::getInstance()->getLuaStack()->getLuaState();
	auto top = lua_gettop(ss);
	lua_getglobal(ss, name);

	lua_call(ss, 1, 0);
	lua_settop(ss, top);
}

void  LuaMgr::endCallGlobal()
{

}

void  LuaMgr::pushString(const char*str)
{
	lua_pushstring(LuaEngine::getInstance()->getLuaStack()->getLuaState(), str);
}*/