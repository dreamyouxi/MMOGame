#include "lua_cmdhandler_auto.hpp"
#include "CmdHandler.h"
#include "tolua_fix.h"
#include "LuaBasicConversions.h"


int lua_cmdhandler_CmdHandler_PushRawData(lua_State* tolua_S)
{
    int argc = 0;
    CmdHandler* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CmdHandler*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cmdhandler_CmdHandler_PushRawData'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        const char* arg0;
        int arg1;

        std::string arg0_tmp; ok &= luaval_to_std_string(tolua_S, 2, &arg0_tmp, "CmdHandler:PushRawData"); arg0 = arg0_tmp.c_str();

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "CmdHandler:PushRawData");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_PushRawData'", nullptr);
            return 0;
        }
        cobj->PushRawData(arg0, arg1);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CmdHandler:PushRawData",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_PushRawData'.",&tolua_err);
#endif

    return 0;
}
int lua_cmdhandler_CmdHandler_PushProtoData(lua_State* tolua_S)
{
    int argc = 0;
    CmdHandler* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CmdHandler*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cmdhandler_CmdHandler_PushProtoData'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        std::string arg0;

        ok &= luaval_to_std_string(tolua_S, 2,&arg0, "CmdHandler:PushProtoData");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_PushProtoData'", nullptr);
            return 0;
        }
        cobj->PushProtoData(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CmdHandler:PushProtoData",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_PushProtoData'.",&tolua_err);
#endif

    return 0;
}
int lua_cmdhandler_CmdHandler_PushCmd(lua_State* tolua_S)
{
    int argc = 0;
    CmdHandler* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CmdHandler*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cmdhandler_CmdHandler_PushCmd'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        Cmd* arg0;

        ok &= luaval_to_object<Cmd>(tolua_S, 2, "Cmd",&arg0, "CmdHandler:PushCmd");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_PushCmd'", nullptr);
            return 0;
        }
        cobj->PushCmd(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CmdHandler:PushCmd",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_PushCmd'.",&tolua_err);
#endif

    return 0;
}
int lua_cmdhandler_CmdHandler_SendCmd(lua_State* tolua_S)
{
    int argc = 0;
    CmdHandler* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CmdHandler*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cmdhandler_CmdHandler_SendCmd'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        Cmd* arg0;

        ok &= luaval_to_object<Cmd>(tolua_S, 2, "Cmd",&arg0, "CmdHandler:SendCmd");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_SendCmd'", nullptr);
            return 0;
        }
        cobj->SendCmd(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CmdHandler:SendCmd",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_SendCmd'.",&tolua_err);
#endif

    return 0;
}
int lua_cmdhandler_CmdHandler_PushDataPack(lua_State* tolua_S)
{
    int argc = 0;
    CmdHandler* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (CmdHandler*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_cmdhandler_CmdHandler_PushDataPack'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        DataPack* arg0;

        ok &= luaval_to_object<DataPack>(tolua_S, 2, "DataPack",&arg0, "CmdHandler:PushDataPack");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_PushDataPack'", nullptr);
            return 0;
        }
        cobj->PushDataPack(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "CmdHandler:PushDataPack",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_PushDataPack'.",&tolua_err);
#endif

    return 0;
}
int lua_cmdhandler_CmdHandler_DestroyInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_DestroyInstance'", nullptr);
            return 0;
        }
        CmdHandler::DestroyInstance();
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "CmdHandler:DestroyInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_DestroyInstance'.",&tolua_err);
#endif
    return 0;
}
int lua_cmdhandler_CmdHandler_GetInstance(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"CmdHandler",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_cmdhandler_CmdHandler_GetInstance'", nullptr);
            return 0;
        }
        CmdHandler* ret = CmdHandler::GetInstance();
        object_to_luaval<CmdHandler>(tolua_S, "CmdHandler",(CmdHandler*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "CmdHandler:GetInstance",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_cmdhandler_CmdHandler_GetInstance'.",&tolua_err);
#endif
    return 0;
}
static int lua_cmdhandler_CmdHandler_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (CmdHandler)");
    return 0;
}

int lua_register_cmdhandler_CmdHandler(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"CmdHandler");
    tolua_cclass(tolua_S,"CmdHandler","CmdHandler","",nullptr);

    tolua_beginmodule(tolua_S,"CmdHandler");
        tolua_function(tolua_S,"PushRawData",lua_cmdhandler_CmdHandler_PushRawData);
        tolua_function(tolua_S,"PushProtoData",lua_cmdhandler_CmdHandler_PushProtoData);
        tolua_function(tolua_S,"PushCmd",lua_cmdhandler_CmdHandler_PushCmd);
        tolua_function(tolua_S,"SendCmd",lua_cmdhandler_CmdHandler_SendCmd);
        tolua_function(tolua_S,"PushDataPack",lua_cmdhandler_CmdHandler_PushDataPack);
        tolua_function(tolua_S,"DestroyInstance", lua_cmdhandler_CmdHandler_DestroyInstance);
        tolua_function(tolua_S,"GetInstance", lua_cmdhandler_CmdHandler_GetInstance);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(CmdHandler).name();
    g_luaType[typeName] = "CmdHandler";
    g_typeCast["CmdHandler"] = "CmdHandler";
    return 1;
}
TOLUA_API int register_all_cmdhandler(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,nullptr,0);
	tolua_beginmodule(tolua_S,nullptr);

	lua_register_cmdhandler_CmdHandler(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

