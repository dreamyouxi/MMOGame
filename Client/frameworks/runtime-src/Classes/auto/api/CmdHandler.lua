
--------------------------------
-- @module CmdHandler
-- @parent_module 

--------------------------------
-- 
-- @function [parent=#CmdHandler] PushRawData 
-- @param self
-- @param #char data
-- @param #int size
-- @return CmdHandler#CmdHandler self (return value: CmdHandler)
        
--------------------------------
-- 
-- @function [parent=#CmdHandler] PushProtoData 
-- @param self
-- @param #string data
-- @return CmdHandler#CmdHandler self (return value: CmdHandler)
        
--------------------------------
-- brief interface to push cmd  for network thread
-- @function [parent=#CmdHandler] PushCmd 
-- @param self
-- @param #Cmd 
-- @return CmdHandler#CmdHandler self (return value: CmdHandler)
        
--------------------------------
-- brief send the cmd to server
-- @function [parent=#CmdHandler] SendCmd 
-- @param self
-- @param #Cmd 
-- @return CmdHandler#CmdHandler self (return value: CmdHandler)
        
--------------------------------
-- brief push data pack to process in lua use cmd_pb.lua to parse<br>
-- c++ just send and recv from server
-- @function [parent=#CmdHandler] PushDataPack 
-- @param self
-- @param #DataPack pack
-- @return CmdHandler#CmdHandler self (return value: CmdHandler)
        
--------------------------------
-- 
-- @function [parent=#CmdHandler] DestroyInstance 
-- @param self
-- @return CmdHandler#CmdHandler self (return value: CmdHandler)
        
--------------------------------
-- 
-- @function [parent=#CmdHandler] GetInstance 
-- @param self
-- @return CmdHandler#CmdHandler ret (return value: CmdHandler)
        
return nil
