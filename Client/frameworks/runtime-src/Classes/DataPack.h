﻿#ifndef __DATAPACK__H__
#define __DATAPACK__H__
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <memory>
#include "zlib.h"
#include "cocos2d.h"
using namespace  cocos2d;
class Cmd;
class DataPack : public Ref
{
public:
	char * buffer = nullptr;
	unsigned int size = 0;
	DataPack(){};
	~DataPack();
	static DataPack*CreateWithCmd(Cmd*cmd, bool encode = false);

	static DataPack*CreateWithMask(int mask);
	static DataPack*CreateWithString(const std::string &data, bool encode = false);
	static DataPack*CreateWithRawData(const char * data, int size,bool encode = false);
	inline bool isEncode()
	{
		return _isEncode;
	}

	/**
	* @brief  encode the buff with zlib
	*/
	void Encode();
	/**
	* @brief decode the buff with zlib
	*/
	void Decode();
	void Alloc(unsigned int size);
	void DeAlloc();

	bool _isEncode = false;

};

#endif