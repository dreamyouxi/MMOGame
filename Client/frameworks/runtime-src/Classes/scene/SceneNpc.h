﻿#pragma once
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <memory>
#include "cocos2d.h"
#include "SceneNpc.h"
#include "cocostudio/CocoStudio.h"
#include "cocostudio/CCArmature.h"
using namespace cocostudio;
using namespace  cocos2d;

class SceneNpc : public Ref
{
public:

	void CreateEntity(std::string);
	  
private:
	Armature * ani_body = nullptr;//动画对象
	std::vector<std::string> _plist_file;//加载的plist文件
	std::string json;//加载的json 文件
	std::string armature_name;//armature_name 名字

	int dir = 0;

	int x = 0;
	int y = 0;
};
