﻿#pragma  once
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <memory>
#include "cocos2d.h"

using namespace  cocos2d;

enum  Item
{

};
/**
 * @brief 场景 地图类
 */
class SceneMap
{
public:

	bool CanWalk(int x, int y)
	{
		return _map[x][y] == 0;
	}

	const int GRID_X = 30;//格子像素大小
	const int GRID_Y = 30;//格子像素大小
private:
	std::string name;

	int _map[1000][1000];
};
