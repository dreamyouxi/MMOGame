﻿#ifndef __SINGLETON__H__
#define __SINGLETON__H__
/*
Email me@dreamyouxi.com
*/
/*
template<class T>
class Singleton
{
public:*/
/**
* @brief return the instance,for thread safe use suport c++11 compiler
*/
/*
static T* GetInstance()
{
//static T ins;return &ins;
if (ins == nullptr)
{
ins = new T();
}
return ins;
}
static void DestroyInstance()
{
if (ins != nullptr)
{
delete ins;
ins = nullptr;
}
}
static T* ins;
};
*/

#define  SingletonCPP(T)\
	static T* ins = nullptr; \
	T* T::GetInstance()\
{\
if (ins == nullptr)\
{\
	ins = new T(); \
}\
	return ins; \
}\
	void T::DestroyInstance()\
{\
if (ins != nullptr)\
{\
	delete ins; \
	ins = nullptr; \
}\
}

#define  SingletonH(T)\
	public:\
	static T* GetInstance(); \
	static void DestroyInstance();

#endif
