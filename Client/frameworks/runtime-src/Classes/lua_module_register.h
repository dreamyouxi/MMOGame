#ifndef __LUA_TEMPLATE_RUNTIME_FRAMEWORKS_RUNTIME_SRC_CLASSES_LUA_MODULE_REGISTER_H__
#define __LUA_TEMPLATE_RUNTIME_FRAMEWORKS_RUNTIME_SRC_CLASSES_LUA_MODULE_REGISTER_H__

#include "lua.h"
#include "Lua-BindingsExport.h"
#include "auto/lua_cmdhandler_auto.hpp"
CC_LUA_DLL  int  lua_module_register(lua_State* L);

#if __cplusplus
extern "C" {
#endif
#include "pb.c"
	/*
	*@brief custom lua module register
	*/
	static int lua_module_register_customs(lua_State*l)
	{
		register_all_cmdhandler(l);
		//pb to lua
		luaopen_pb(l);
		return 0;
	}
#if __cplusplus
}
#endif



#endif  // __LUA_TEMPLATE_RUNTIME_FRAMEWORKS_RUNTIME_SRC_CLASSES_LUA_MODULE_REGISTER_H__