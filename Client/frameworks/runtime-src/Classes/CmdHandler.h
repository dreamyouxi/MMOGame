﻿#ifndef __CMDHANDLER__H__
#define __CMDHANDLER__H__
/*
Email me@dreamyouxi.com
*/
#include <string>
#include "Singleton.hpp"
#include "cocos2d.h"
using namespace  cocos2d;
class DataPack;
class Cmd;
class CmdHandler 
{
	SingletonH(CmdHandler)
public:
	/**
	 * @brief send the cmd to server
	 */
	void SendCmd(Cmd *);

	/**
	 * @brief interface to push cmd  for network thread
	 */
	void PushCmd(Cmd*);
	/**
	 * @brief push data pack to process in lua use cmd_pb.lua to parse
	 * c++ just send and recv from server
	 */
	void PushDataPack(DataPack*pack);

	void PushProtoData(const std::string &data);
	void PushRawData(const char *data, int size);
};

#endif
