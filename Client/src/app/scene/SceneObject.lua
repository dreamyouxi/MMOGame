--[[
Email me@dreamyouxi.com
]]
local M= class("SceneObject");
local ani_data = require("app.data.Animation");
function M:ctor(root)
   -- local root = cc.Director:getInstance():getRunningScene();
    self.x=0;
    self.y=0;
    self.target_x=0;
    self.target_y=0;

    self.thisid=0;
    self.name="SceneObject0";
    self.sp_body=nil;
    self.dir=0;

    local path = "ani/";
    local cache = cc.SpriteFrameCache:getInstance()
    local data = ani_data["p_10_w"];
    for k,v in pairs(data.plist) do
        cache:addSpriteFrames(path .. v);
    end
    ccs.ArmatureDataManager:getInstance():addArmatureFileInfo( path ..  data.json)

    local armature = ccs.Armature:create(data.name)
    armature:getAnimation():playWithNames({"m_01_w_0"})
    armature:setPosition(1136 / 2 - 300, 640 / 2 - 200);
   -- armature:runAction(cc.MoveTo:create(7,cc.p(1136,640)))
    root:addChild(armature)
    armature:getAnimation():setSpeedScale(0.2)
    self.sp_body = armature;
end

function M:moveTo(x,y)
    self.target_x =x;
    self.target_y=y;

end

function M:update()
    self.x = self.sp_body:getPositionX();
    self.y = self.sp_body:getPositionY();
    if  math.abs(self.target_x-self.x) <2 and  math.abs(self.target_y-self.y) <2 then

        return;
    end
    self.sp_body:setPosition(self.x+0.4 ,self.y + 0.4);
end

function M:playAnimation()


end

cc.exports._class.SceneObject = M
return M;