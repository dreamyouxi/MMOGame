--[[
Email me@dreamyouxi.com
]]
local M= class("SceneMgr");
local SceneObject = require("app.scene.SceneObject")
local SceneMap = require("app.scene.SceneMap")

function M:ctor(root)
    self.scene_root = root;
    self.ui_root = cc.Node:create();
    self.ui_root:setAnchorPoint(cc.p(0,0));
    root:addChild(self.ui_root,10000);

    local map = SceneMap.new(root);
    self.map = obj;
    local obj = SceneObject.new(root);
    self.obj = obj;
end

function M:update()

end

function M:getSceneRoot()
    return self.scene_root;
end

function M:getUiRoot()
    return self.ui_root;
end
cc.exports._class.SceneMgr = M
return M;