-- Generated By protoc-gen-lua Do not Edit
local protobuf = require "protobuf"
module('cmd_pb')


local CMD = protobuf.Descriptor();
local CMD_ID_FIELD = protobuf.FieldDescriptor();
local CMD_NAME_FIELD = protobuf.FieldDescriptor();
local CMD_DATA_FIELD = protobuf.FieldDescriptor();

CMD_ID_FIELD.name = "id"
CMD_ID_FIELD.full_name = ".cmd.id"
CMD_ID_FIELD.number = 1
CMD_ID_FIELD.index = 0
CMD_ID_FIELD.label = 1
CMD_ID_FIELD.has_default_value = false
CMD_ID_FIELD.default_value = 0
CMD_ID_FIELD.type = 13
CMD_ID_FIELD.cpp_type = 3

CMD_NAME_FIELD.name = "name"
CMD_NAME_FIELD.full_name = ".cmd.name"
CMD_NAME_FIELD.number = 2
CMD_NAME_FIELD.index = 1
CMD_NAME_FIELD.label = 1
CMD_NAME_FIELD.has_default_value = false
CMD_NAME_FIELD.default_value = ""
CMD_NAME_FIELD.type = 9
CMD_NAME_FIELD.cpp_type = 9

CMD_DATA_FIELD.name = "data"
CMD_DATA_FIELD.full_name = ".cmd.data"
CMD_DATA_FIELD.number = 3
CMD_DATA_FIELD.index = 2
CMD_DATA_FIELD.label = 1
CMD_DATA_FIELD.has_default_value = false
CMD_DATA_FIELD.default_value = ""
CMD_DATA_FIELD.type = 12
CMD_DATA_FIELD.cpp_type = 9

CMD.name = "cmd"
CMD.full_name = ".cmd"
CMD.nested_types = {}
CMD.enum_types = {}
CMD.fields = {CMD_ID_FIELD, CMD_NAME_FIELD, CMD_DATA_FIELD}
CMD.is_extendable = false
CMD.extensions = {}

cmd = protobuf.Message(CMD)

