--[[
Email me@dreamyouxi.com

全局网络消息回调
]]
require("person_pb")
require("cmd_pb")

require("scene_pb")

local cmd_id_to_name = {
    [1] = "",

};

local cmd_name_to_id = {
    [""] = 1,

};

local function parse(t)
    if t then
        print(tostring(t));
        local msg = person_pb.Person();
        local msg1 = person_pb.Person()
        msg1:ParseFromString(t)
    else
        print(" t is nil")
    end
end

local function parse_cmd_pb(t)
    if t then
        local cmd = cmd_pb.cmd();
        cmd:ParseFromString(t)
        local id = cmd.id;
        local data = cmd.data;
        local name = cmd.name;
        print("recv: name=" .. name);
    else
        print(" t is nil")
    end
end
--接收来自c++的 proto消息
function PARSE_PROTO_MSG(t)
    xpcall( function()
        parse(t);
    end , function(msg)
        local msg = debug.traceback(msg)
        print("LuaCmd Error:" .. msg);
        return msg
    end )
end

--接收来自c++的 proto消息
function PARSE_PROTO_CMD_MSG(t)
    xpcall( function()
        parse_cmd_pb(t);
    end , function(msg)
        local msg = debug.traceback(msg)
        print("LuaCmd Error:" .. msg);
        return msg
    end )
end

--lua 层 发起proto 接口
function SEND_PROTO_MSG(t)

    local name = t.__name__

    local data = t:SerializeToString();
    local id = cmd_name_to_id[name];

    local cmd = cmd_pb.cmd();
    cmd.name = name;
    cmd.data = data;
    local cmd_data = cmd:SerializeToString();

    -- call c++ interface to send
    CmdHandler:GetInstance():PushProtoData(cmd_data);
    -- CmdHandler:GetInstance():PushRawData(cmd_data, string.len(cmd_data));

    print("LuaCmd:send cmd name=" .. name);
end
