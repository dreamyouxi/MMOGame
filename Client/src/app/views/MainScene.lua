--[[
Email me@dreamyouxi.com
]]

local MainScene = class("MainScene", cc.load("mvc").ViewBase)
local SceneMgr = require("app.scene.SceneMgr")

require("scene_pb")

MainScene.RESOURCE_FILENAME = "MainScene.csb"

function MainScene:update()
    g_SceneMgr:update();
end

function MainScene:onCreate()
    --[[ you can create scene with following comment code instead of using csb file.
    -- add background image
    display.newSprite("HelloWorld.png")
        :move(display.center)
        :addTo(self)

    -- add HelloWorld label
    cc.Label:createWithSystemFont("Hello World", "Arial", 40)
        :move(display.cx, display.cy + 200)
        :addTo(self)
    ]]

    local root = self:getResourceNode();
    root:removeAllChildren();
    --local map = cc.TMXTiledMap:create("maptest/wyt_map.tmx")
    --root:addChild(map, 0)
    
  
    --Listener
    local lis = cc.EventListenerTouchOneByOne:create()
    lis:registerScriptHandler(function (touch, event)
           local target = event:getCurrentTarget()
        local x,y = target:getPosition()
        self.obj:moveTo(x,y)
        return true
    end ,cc.Handler.EVENT_TOUCH_ENDED)

   lis:registerScriptHandler(function (touch, event)
        print(tostring(event));
        return true
    end ,cc.Handler.EVENT_TOUCH_MOVED)

   lis:registerScriptHandler(function (touch, event)
        print(tostring(event));
        return true
    end ,cc.Handler.EVENT_TOUCH_BEGAN)

    local eventDispatcher = root:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(lis, self)

    root:runAction(cc.RepeatForever:create(cc.Sequence:create(cc.DelayTime:create(0),cc.CallFunc:create(function()
    
    self:update();
    end))));

    cc.exports.g_SceneMgr = SceneMgr.new(root);

    local pb = scene_pb.scene_enter();
    pb.id = math.floor(math.random() * 10000 % 1000);
    pb.name = "www";
 --armature:setScaleX(-1);
    SEND_PROTO_MSG(pb);
end

return MainScene
