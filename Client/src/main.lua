--[[
Email me@dreamyouxi.com
]]
cc.FileUtils:getInstance():setPopupNotify(false)
cc.FileUtils:getInstance():addSearchPath("src/")
cc.FileUtils:getInstance():addSearchPath("res/")
cc.FileUtils:getInstance():addSearchPath("src/app/net/ProtoLua")
cc.FileUtils:getInstance():addSearchPath("src/app/net/ProtoMsg")
cc.FileUtils:getInstance():addSearchPath("src/app/net")

-- for exe run
cc.FileUtils:getInstance():addSearchPath("../../src/app")
cc.FileUtils:getInstance():addSearchPath("../../src/")
cc.FileUtils:getInstance():addSearchPath("../../res/")
cc.FileUtils:getInstance():addSearchPath("../../src/app/net/ProtoLua")
cc.FileUtils:getInstance():addSearchPath("../../src/app/net/ProtoMsg")
cc.FileUtils:getInstance():addSearchPath("../../src/app/net")

require "config"
require "cocos.init"
require "LuaCmd"
require "app.base.LuaGlobal"

local function main()
    cc.exports._class={};
    require("app.MyApp"):create():run()
end

local status, msg = xpcall(main, __G__TRACKBACK__)
if not status then
    print(msg)
end
