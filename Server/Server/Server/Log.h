﻿#ifndef __LOG__
#define __LOG__
/*
Email me@dreamyouxi.com
*/
#include <string>

enum class  LogType
{
	INFO,
	DEBUG,
	WARN,
	FATAL,
	ERROR1,
};
void SET_COLOR_DEBUG();
void SET_COLOR_INFO();
void SET_COLOR_WARN();
void SET_COLOR_ERROR();
void SET_COLOR_FATAL();

void LOG(LogType type, const char *name, std::string  what);

#define LOG_DEBUG(what) do {SET_COLOR_DEBUG(); LOG(LogType::DEBUG,LOG_NAME, what ) ;} while(0)
#define LOG_INFO(what) do { SET_COLOR_INFO(); LOG(LogType::INFO,LOG_NAME, what); } while(0)	
#define LOG_WARN(what) do {SET_COLOR_WARN();LOG(LogType::WARN,LOG_NAME, what); } while(0)
#define LOG_ERROR(what) do {SET_COLOR_ERROR(); LOG(LogType::ERROR1,LOG_NAME, what) ;} while (0)
#define LOG_FATAL(what) do {SET_COLOR_FATAL(); LOG(LogType::FATAL,LOG_NAME, what);} while (0)

#define LOG_DEBUGF(what) do {SET_COLOR_DEBUG();LOG(LogType::DEBUG,LOG_NAME,( what).str() ) ;} while(0)
#define LOG_INFOF(what) do {SET_COLOR_INFO(); LOG(LogType::INFO,LOG_NAME, (what).str()); } while(0)	
#define LOG_WARNF(what) do {SET_COLOR_WARN();LOG(LogType::WARN,LOG_NAME, (what).str()); } while(0)
#define LOG_ERRORF(what) do {SET_COLOR_ERROR(); LOG(LogType::ERROR1,LOG_NAME,( what).str()) ;} while (0)
#define LOG_FATALF(what) do {SET_COLOR_FATAL();LOG(LogType::FATAL,LOG_NAME, (what).str());} while (0)

#include <boost/format.hpp>
boost::format FMT(const std::string & str);

#endif
