﻿#include "Log.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "windows.h"
//TODO add multi thread suport and log to file
void SET_COLOR_DEBUG()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
}
void SET_COLOR_INFO()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_GREEN);
}
void SET_COLOR_WARN()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE);
}
void SET_COLOR_ERROR()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN);
}
void SET_COLOR_FATAL()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);
}


void LOG(LogType type, const char *name, std::string  what)
{
	//OutputDebugString(log_buffer);
	time_t rawtime;
	struct tm * t;
	time(&rawtime);
	t = localtime(&rawtime);

	//printf("[%d-%d-%d] [%d:%d:%d] : ", t->tm_year + 1900, t->tm_mon, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec);

	printf("[%d:%d:%d] %s INFO:%s\n", t->tm_hour, t->tm_min, t->tm_sec, name, what.c_str());
}

boost::format FMT(const std::string & str)
{
	boost::format fmter(str);
	return fmter;
}