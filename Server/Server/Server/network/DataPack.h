﻿#ifndef __DATAPACK__H__
#define __DATAPACK__H__
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <memory>
namespace Network
{
	class Cmd;
	class DataPack
	{
	public:
		char * buffer = nullptr;
		unsigned int size = 0;

		~DataPack();
		static DataPack*CreateWithCmd(Cmd*cmd, bool encode = false);

		static DataPack*CreateWithMask(int mask);
		static DataPack*CreateWithString(const std::string &data, bool encode = false);

		inline bool isEncode()
		{
			return _isEncode;
		}
	private:
		/**
		* @brief  encode the buff with zlib
		*/
		void Encode();
		/**
		* @brief decode the buff with zlib
		*/
		void Decode();
		void Alloc(unsigned int size);
		void DeAlloc();

		bool _isEncode = false;
		DataPack(){};
	};
}
#endif