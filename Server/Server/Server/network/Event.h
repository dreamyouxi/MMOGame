#ifndef __NETWORK_EVENT__
#define __NETWORK_EVENT__
/*
Email me@dreamyouxi.com
*/
#include "../scene/SceneNpc.h"
#include "../scene/GameScene.h"
#include "../base/EventDelegate.h"
using namespace Scene;
using namespace Base;
namespace Network
{
	struct   EventPair
	{
		const char *name;
		const EventCallBackFunc * func;
		EventPair(const char *_name, EventCallBackFunc _func) :name(_name), func(_func){};
		EventPair() :name(nullptr), func(nullptr){};
	};

	//网络消息注册
	static EventPair Events[1000] =
	{//注册方法第一个为消息名字，第二个为消息处理的函数指针
		EventPair("scene_req_move", &SceneNpcHandler::req_move),
		EventPair("scene_enter", &GameSceneHandler::scene_enter)




	};
//	#define  EVETNS_LEN (sizeof(Events) / sizeof(EventPair))








	/*	enum  class Type
		{
		PROTO_MSG,
		DISCONNECTED,
		CONNECTED,
		NULL,
		};
		class DataPack;

		class Event
		{
		public:
		const Type & GetType();
		DataPack*GetData();

		private:
		Type type = Type::NULL;
		DataPack *data = nullptr;
		};
		*/

}

#endif
