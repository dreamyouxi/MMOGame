﻿#ifndef __NETWORK_EVENT__
#define __NETWORK_EVENT__
/*
Email me@dreamyouxi.com
*/

namespace Network
{
	enum  class Type
	{
		PROTO_MSG,
		DISCONNECTED,
		CONNECTED,
		NULL
	};
	class DataPack;
	class Event
	{
	public:
		const Type & GetType();
		DataPack*GetData();

	private:
		Type type = Type::NULL;
		DataPack *data = nullptr;
	};


}


#endif
