﻿#ifndef __CMD__H__
#define __CMD__H__
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <queue>
#include <memory>
#include "cocos2d.h"
using namespace  cocos2d;
class DataPack;

class Cmd
{
public:
	unsigned short id = 0;
	char *buffer = nullptr;
	/**
	 * @brief gen the cmd with data pack
	 */
	void Parse(DataPack *pack);

	void DeAlloc()
	{
		CC_SAFE_DELETE_ARRAY(buffer);
	}
};

#endif
