﻿#ifndef __SESSION_H__
#define __SESSION_H__
/*
Email me@dreamyouxi.com
*/
#include <functional>
#include <unordered_map> 
#include <boost/asio/ip/tcp.hpp> 
#include <boost/asio/spawn.hpp>  
#include <boost/asio/strand.hpp>
#include "../base/ThreadSafeQueue.hpp"
class AsioServer;
class Session
{
	using socket = boost::asio::ip::tcp::socket;
	using yield_context = boost::asio::yield_context;
	using strand = boost::asio::io_service::strand;
public:
	void Disconnect();
	Session(AsioServer* server, int id);
	void Run();
	socket &GetSocket()
	{
		return _socket;
	}
	bool IsConnected()
	{
		return _isConnected;
	}
	inline int GetId()
	{
		return _id;
	}
private:
	void LoopRead(yield_context yield);
	void LoopWrite(yield_context yield);
	void SleepMS(unsigned int ms, yield_context yield);

	AsioServer* _server;
	strand _strand;
	socket _socket;
	bool _isConnected = false;

	Base::ThreadSafeQueue<std::string> _sendQueue;
	Base::ThreadSafeQueue<std::string> _recvQueue;
	int _id = 0;
};



#endif
