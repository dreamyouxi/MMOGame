﻿#ifndef __ASIOSERVER_H__
#define __ASIOSERVER_H__
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <unordered_map>
#include <vector>
#include <boost/asio/io_service.hpp> 
#include <boost/asio/spawn.hpp> 

class AsioServer
{
	using yield_context = boost::asio::yield_context;
	using io_service = boost::asio::io_service;
public:
	void Run(int port);

	void LoopAccept(int   port, yield_context yield);
	io_service io;

};





#endif
