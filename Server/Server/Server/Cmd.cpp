﻿#include "Cmd.h"
#include "DataPack.h"
#include <iostream>
#include "cocos2d.h"
using namespace  cocos2d;
using namespace std;

void Cmd::Parse(DataPack*pack)
{
	if (this->buffer)
	{
		this->DeAlloc();
	}
	if (pack)
	{
		//前2个字节为cmd 的id  后面为pb 序列化的string
		unsigned short id = 0;
		memset(&id, 0, sizeof(unsigned short));
		memcpy(&id, pack->buffer, 2);
		this->buffer = new char[pack->size - 2];
		memcpy(this->buffer, pack->buffer + 2, pack->size - 2);

		pack->release();
	}
}
