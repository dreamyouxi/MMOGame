﻿#include "EventDispatcher.h"
#include <iostream>
#include "../network/Event.h"
namespace Base
{
	using namespace std;
	SingletonCPP(EventDispatcher);

	void EventDispatcher::AutoRegisterAllStaticEvents()
	{
		_map_static.clear();
		for (int i = 0; i < 100; i++)
		{
			auto &e = Network::Events[i];
			if (e.func == nullptr)break;
			this->AddStaticEvent(e.name, (e.func));
		}
	}

	void EventDispatcher::ProcessOneLoop()
	{
		//处理 静态消息的输入队列
		this->ProcessStaticEventQueue();
	}

	void EventDispatcher::AddStaticEvent(const std::string & name, EventCallBackFunc *cb)
	{
		this->_map_static[name] = cb;
	}
	void EventDispatcher::ProcessStaticEventQueue()
	{
		Queue q;
		{
			_queue_recv_static.lock();
			auto &	 safe = _queue_recv_static.GetUnsafeQueue();
			q.swap(safe);
			_queue_recv_static.unlock();
		}

		while (q.empty() == false)
		{
			auto &v = q.front();
			auto&it = _map_static.find(v.first);
			if (it == _map_static.end())
			{
				q.pop();
				return;
			}
			auto & func = it->second;
			func(nullptr, v.second);
			q.pop();
		}

	}
}