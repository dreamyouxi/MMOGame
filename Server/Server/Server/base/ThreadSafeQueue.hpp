﻿#ifndef __BASE_THREADSAFEQUEUE__HPP__
#define __BASE_THREADSAFEQUEUE__HPP__
/*
Email me@dreamyouxi.com
*/
#include <queue>
#include <mutex>

namespace Base
{

	template< typename T>
	class ThreadSafeQueue
	{
#define  LOCK(WHAT) std::lock_guard<std::mutex> locker(WHAT);

	public:
		void pop()
		{
			LOCK(_mutex);
			_queue.pop();
		}
		void push(const T &t)
		{
			LOCK(_mutex);
			_queue.push(t);
		}
		const  T &  front()
		{
			LOCK(_mutex);
			return _queue.front();
		}
		bool empty()
		{
			LOCK(_mutex);
			bool ret = _queue.empty();
			return ret;
		}
		std::queue<T> &  GetUnsafeQueue()
		{
			return _queue;
		}
		void lock()
		{
			_mutex.lock();
		}
		void unlock()
		{
			_mutex.unlock();
		}
	private:
		std::mutex _mutex;
		std::queue<T> _queue;
	};

};
#endif
