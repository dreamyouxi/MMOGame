﻿#ifndef __BASE_EVENTDELEGATE__
#define __BASE_EVENTDELEGATE__
/*
Email me@dreamyouxi.com
*/
#include <string>

/**
 * @brief base class for all event delegate what you want to listen
 *
 */
namespace Network
{
	class DataPack;
}
namespace Base
{

	//todo use MFC's message MAP instead of registerevent
	typedef  Network::DataPack* EventCallBackFunc(void*obj, const std::string & pb);

	class EventDelegate
	{
	public:
		void RegisterEvent(const std::string &event_name ,EventCallBackFunc func);
		void RegisterEvent(int event_id, EventCallBackFunc func);
	};

}


#endif
