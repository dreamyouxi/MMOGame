﻿#ifndef __SCENE_GAMESCENE__
#define __SCENE_GAMESCENE__
/*
Email me@dreamyouxi.com
*/
#include"../base/Object.h"
#include <string>
#include <unordered_map>
#include "../base/Singleton.hpp"
#include "../base/EventDelegate.h"
namespace Scene
{
	using namespace Base;
	namespace GameSceneHandler
	{
		using  namespace Network;
		 DataPack*scene_enter(void *obj, const std::string &p);
	}


	class SceneObject;
	class GameScene : public Base::Object
	{
		SingletonH(GameScene)










	public:
		void AddSceneObject(SceneObject*obj);
		void ProcessOneLoop();
		std::string name = "Default";

	private:
		std::unordered_map<long long, SceneObject* > _objs;

	};


}


#endif
