﻿#ifndef __SCENE_SCENEOBJECT__
#define __SCENE_SCENEOBJECT__
/*
Email me@dreamyouxi.com
*/
#include"../base/Object.h"
#include <string>
namespace Scene
{
	class SceneObject : public Base::Object
	{
	public:
		int x=0;
		int y=0;
		std::string name="SceneObject";
		long long thisid = 0;
		void Save();
	};


}


#endif
