﻿#ifndef __SCENE_MAPBASE__
#define __SCENE_MAPBASE__
/*
Email me@dreamyouxi.com
*/
#include <string>
#include <vector>
#include "../base/Object.h"

namespace Scene
{
	class MapBlock
	{
	public:
		enum class Type
		{
			WALKABLE,
			NOTAPPROACH,
		};
	public:
		Type type =Type:: WALKABLE;
	};

	class MapBase : public Base::Object
	{
	public:
		void LoadMap(const  std::string &name);
		bool WalkAble(int x, int y);
	
	protected:
		std::string _name;
	//	std::vector<std::vector<MapBlock*>> _mapBlock;
		MapBlock* _mapBlock[1000][1000];
	};
};


#endif
