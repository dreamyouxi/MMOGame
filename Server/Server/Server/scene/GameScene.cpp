﻿#include "GameScene.h"
#include "../protobuf/scene.pb.h"
#include "SceneObject.h"
#include "SceneNpc.h"
#include <iostream>
#include "../Log.h"
const char LOG_NAME[] = "GameScene";

using namespace std;
namespace Scene
{
	SingletonCPP(GameScene);
	namespace GameSceneHandler
	{
		using  namespace Network;
		DataPack*scene_enter(void *obj, const std::string &p)
		{//处理请求进入场景消息
			scene::scene_enter pbb;
			pbb.ParseFromString(p);

			int id = pbb.id();
			string name = pbb.name();

			SceneNpc *npc = new SceneNpc;
			npc->thisid = id;
			npc->name = name;
			
			GameScene::GetInstance()->AddSceneObject(npc);
			return nullptr;
		}
	}

	void GameScene:: AddSceneObject(SceneObject*obj)
	{
		LOG_INFOF(FMT("添加了角色 %d  %s") % obj->thisid % obj->name);
		this->_objs[obj->thisid] = obj;
	}
	void GameScene::ProcessOneLoop()
	{



	}

}