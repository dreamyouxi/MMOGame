﻿#include "SceneNpc.h"
#include <iostream>
#include "../protobuf/scene.pb.h"
#include "../network/DataPack.h"
using namespace std;

#define  CMD_SCENE_REQ_MOVE "scene_req_move"

namespace Scene
{
	namespace SceneNpcHandler
	{
		using  namespace Network;
		DataPack*req_move(void *obj, const std::string &p)
		{
			if (!obj)return nullptr;
			scene::scene_req_move pb;
			pb.ParseFromString(p);
			SceneNpc *npc = static_cast<SceneNpc*>(obj);

			npc->MoveTo(pb.x(), pb.y());

			cout << "req_move  x=" << pb.x() << "  y=" << pb.y() << endl;

			return 0;
		}
	}



	void SceneNpc::Reg()
	{
		RegisterEvent(CMD_SCENE_REQ_MOVE, SceneNpcHandler::req_move);
	}
}


