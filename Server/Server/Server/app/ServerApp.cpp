﻿#include "ServerApp.h"
#include "../base/EventDispatcher.h"
#include "../scene/GameScene.h"
#include <iostream>
#include "windows.h"
namespace App
{
	using namespace Base;
	using namespace Scene;
	int ServerApp::Run()
	{
		EventDispatcher::GetInstance()->AutoRegisterAllStaticEvents();
		while (true)
		{
			Sleep(1);
			EventDispatcher::GetInstance()->ProcessOneLoop();
			GameScene::GetInstance()->ProcessOneLoop();
		}
		return 0;
	}
}