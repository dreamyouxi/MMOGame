#include <fstream>
#include <iostream>
#include <vector>
#include <math.h>

#include <iostream>
#include <memory>
#include <array>
#include "asio/AsioServer.h"
#include <thread>

#include "app/SceneApp.h"
using namespace  std;
#include "Log.h"
 

const char LOG_NAME[] = "main";

int main(int argc, char* argv[])
{
	thread thread_asio([]()
	{
		AsioServer  s;
		s.Run(8111);
	});

	LOG_INFO("asio start up ok");
 
	thread_asio.detach();

	App::SceneApp app;
	app.Run();


 
	return 0;
}
